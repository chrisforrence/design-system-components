import Vue from 'vue'

// Import all components
import DsComment from './Comment.vue'
import DsTitle from './Title.vue'
import DsMarkdown from './Markdown.vue'
import DsColorSwatch from './ColorSwatch.vue'
import DsColorContrast from './ColorContrast.vue'

// For Markdown.vue
import 'github-markdown-css/github-markdown.css'

const Components = {
  DsComment,
  DsTitle,
  DsMarkdown,
  DsColorSwatch,
  DsColorContrast
}

Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name])
})

export default Components
